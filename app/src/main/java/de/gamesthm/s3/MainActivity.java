package de.gamesthm.s3;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.crashlytics.android.Crashlytics;
import com.parse.ParseAnalytics;

import org.altbeacon.beacon.Beacon;

import java.io.File;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import de.gamesthm.s3.Beacon.BeaconController;
import de.gamesthm.s3.Service.PushService;
import de.gamesthm.s3.Service.UserLocationService;
import io.fabric.sdk.android.Fabric;

/**
 * Main Activity of the App.
 */


public class MainActivity extends AppCompatActivity {

    private final static int REQUEST_ENABLE_BT = 10;

    BeaconController bc;
    UserLocationService ulc;
    public static WebView mweb;
    private PushService pushService;
    private ArrayList<Beacon> oldBeacons = new ArrayList<Beacon>();
    private BluetoothAdapter mBluetoothAdapter;
    /**
     * Android standard method to create the activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

//        ParseObject testObject = new ParseObject("TestObject");
//        testObject.put("foo", "bar");
//        testObject.saveInBackground();


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ulc = UserLocationService.getInstance(getApplicationContext());

        mweb = (WebView) findViewById(R.id.webView2);
        mweb.getSettings().setJavaScriptEnabled(true);
        mweb.getSettings().setDomStorageEnabled(true);
        mweb.getSettings().setAllowContentAccess(true);
        mweb.loadUrl("http://george.mnd.thm.de/" + BuildConfig.devURL + "main.html");
        Log.d("3", "loadedURL: " + mweb.getUrl());
        bc = BeaconController.getInstance(this);
        bc.mMain = this;
        mweb.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                bc.addUUID("f0018b9b-7509-4c31-a905-1a27d39c003c");
                bc.addBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25");
                bc.startSearchingIfNeeded();
            }
        });
        Fabric.with(this, new Crashlytics());

        pushService = new PushService(this);

        sendNotification("you're fucked", "error");

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            //TODO: show "doesnt work" Code & Exit App
            sendNotification("you're fucked", "error");
        }


        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        startUpdate();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void sendNotification(String msg, String type) {
        final String webUrl = "javascript:$.notify('" + msg + "','" + type + "')";
        Log.d("notify", webUrl);
        mweb.loadUrl(webUrl);
    }

    /*


    func jsClear() {
        webView.evaluateJavaScript("clearLocations()") { (result, error) in
            if error != nil {
                print(result)
            }
        }
        NSLog("%@", "clearLocations()")

    }

    func jsSendNotification(text: String, type: String){
        var script = "$.notify('"
        script += text
        script += "','"
        script += type
        script += "')"

        NSLog("Notification: "+"%@", script)
        webView.evaluateJavaScript(script) { (result, error) in
            if error != nil {
                print(result)
            }
        }
    }

     */

    /**
     * id1: uuid id2: mayor id3: minor
     * @param beacons
     */
    public void onBeaconInRange(final Collection<Beacon> beacons) {
        try {
            Log.d("Main", beacons.toString());
            //die aktuellen Beacons aus den alten entfernen ->> nur noch die verlorenene (zu weit entfernten)
            oldBeacons.removeAll(beacons);

            //remove verloreneBeacons
            Iterator<Beacon> oldItter = oldBeacons.iterator();
            while (oldItter.hasNext()) {
                final Beacon current = oldItter.next();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final String webUrl = "javascript:removeLocation('" + current.getId1().toString() + "','" + current.getId2().toString() + "','" + current.getId3().toString() + "')";
                        mweb.loadUrl(webUrl);
                    }
                });
            }

            //clear oldBeacons
            oldBeacons.clear();

            Iterator<Beacon> it = beacons.iterator();
            while (it.hasNext()) {
                final Beacon current = it.next();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final String webUrl = "javascript:addLocation('" + current.getId1().toString() + "','" + current.getId2().toString() + "','" + current.getId3().toString() + "')";
                        mweb.loadUrl(webUrl);
                    }
                });
            }
            oldBeacons.addAll(beacons);
        } catch (Exception ex) {
            Log.e("Main", "Unknown Error: ", ex);
        }
    }

    @Override
    public void onDestroy() {
        clearApplicationData();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        //TODO: release the kraken (and some shit), was wir im Hintergrund nicht brauchen

    }

    @Override
    public void onResume() {
        //TODO: wahrscheinlich noch mehr Methoden, bei Rückkehren reload usw.
        super.onResume();
    }

    public void startUpdate() {
        LocationUpdater dc = new LocationUpdater();
        dc.execute();
    }

    /**
     * Method used to clear the application cache
     */
    public void clearApplicationData() {
        File cache = getCacheDir();
        if (cache != null && cache.isDirectory()) {
            cache.delete();
        }
    }

    private class LocationUpdater extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
        }

        /**
         * @param params
         * @return null
         */
        @Override
        protected Void doInBackground(Void... params) {
            //check GPS activated
            if (!ulc.isGPSOn()) {
                //TODO warn User (sendNotification funktioniert nicht, weil anderer Thread)
                //FIXME: richtig unschön (da es nicht zurückkehrt, aber nen Anfang)
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
            synchronized (this) {
                while (!this.isCancelled()) {
                        try {
                            Log.d("GPS", "Update User Location");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Location loc = ulc.getCurrentUserLocation();
                                    if (loc != null) {
                                        final String webUrlLoc = "javascript:setUserLocation('" + loc.getLatitude() + "','" + loc.getLongitude() + "')";
                                        System.out.println("User Location: " + webUrlLoc);
                                        mweb.loadUrl(webUrlLoc);
                                    }
                                }
                            });
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                        }
                }
            }
            return null;
        }

        //Update the progress
        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        //after executing the code in the thread
        @Override
        protected void onPostExecute(Void result) {
        }
    }
}