package de.gamesthm.s3.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parse.GcmBroadcastReceiver;
import com.parse.ParsePushBroadcastReceiver;

import de.gamesthm.s3.Utils.Alert;

/**
 * @author Dustin
 */
public class PushService extends ParsePushBroadcastReceiver{
    final private String TAG = this.getClass().getName();
    private Alert alert = new Alert();
    private Context con;

    public  PushService(Context con){
        this.con = con;
    }

    @Override
    public void onPushOpen(Context context, Intent intent){
        if(intent == null){
            Log.d(TAG, "onReceive: Receiver intent null");
            return;
        }
        String message = intent.getDataString();
        Log.d(TAG, "onReceive: "+message);
        alert.alertOK(TAG,message,con);
    }

    @Override
    public void onReceive(Context context, Intent intent){
        if(intent == null){
            Log.d(TAG, "onReceive: Receiver intent null");
            return;
        }
        String message = intent.getDataString();
        Log.d(TAG, "onReceive: "+message);
        alert.alertOK(TAG,message,con);

    }

}
