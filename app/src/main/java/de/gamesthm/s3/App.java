package de.gamesthm.s3;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Debug;
import android.preference.PreferenceManager;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.parse.SaveCallback;

import de.gamesthm.s3.Utils.Alert;

/**
 * @author Dustin
 */
public class App extends Application {

    private static App instance;
    private Alert alert = new Alert();

    public App() {
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("george.games.thm.de")
                .server("http://george.mnd.thm.de:1337/parse/")

                .build()
        );
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
        // defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);


        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("GCMSenderId", "687982849604");

        installation.saveInBackground();

        //String deviceToken = ParseInstallation.getCurrentInstallation().getInstallationId();
        //Log.d("3", "deviceToken: " + deviceToken);
        //webSettings.setUserAgentString(userAgent + " ||" + deviceToken);

    }



}
