package de.gamesthm.s3.Beacon;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;


import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import de.gamesthm.s3.MainActivity;

/**
 * Created by Benedikt on 30.10.2014.
 */
public class BeaconController implements BeaconConsumer, MonitorNotifier, RangeNotifier {

    private final static String TAG = BeaconController.class.getName();
    public MainActivity mMain;
    private Context mContext;
    private Vector<String> uuids = new Vector<>();
    private Vector<Region> regions = new Vector<>();
    private Vector<String> beaconLayouts = new Vector<>();
    private boolean started = false;
    private BeaconManager beaconManager;
    public static BeaconController INSTANCE;
    private final static int MAXEVENTS = 5;
    private ArrayList<Collection<Beacon>> mLastFoundBeacons;

    public static BeaconController getInstance(Context ctx) {
        if (INSTANCE == null) INSTANCE = new BeaconController(ctx);
        return INSTANCE;
    }

    private BeaconController(Context context) {
        mContext = context;
        INSTANCE = this;
    }

    public void addUUID(String uuid){
        try{
            uuids.add(uuid);
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void addUUIDs(List<String> uuids){
        try{
            for(String uuid: uuids){
                addUUID(uuid);
            }
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void notifyUUIDsChanged(){
        try{
            if(started){ //restart
                this.stopSearching();
            }
            this.startSearchingIfNeeded();
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void setContext(Context ctx) {
        try{
            this.mContext = ctx;
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }


    public void addBeaconLayout(String beaconLayout){
        try{
            beaconLayouts.add(beaconLayout);
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void addBeaconLayouts(List<String> beaconLayouts){
        try{
            for(String beaconLayout: beaconLayouts){
                addBeaconLayout(beaconLayout);
            }
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void notifyBeaconControllerIsSetup(){
        try{
            if(started){ //restart
                this.stopSearching();
            }
            this.startSearchingIfNeeded();
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void notifyBeaconLayoutChanged(){
        try{
            if(started){ //restart
                this.stopSearching();
            }
            this.startSearchingIfNeeded();
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }
    public void startSearchingIfNeeded() {
        try{
            if (started) return;
            if (beaconManager == null) {
                beaconManager = BeaconManager.getInstanceForApplication(mContext);
            }
            for(String beaconLayout: beaconLayouts){
                beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(beaconLayout));
            }
            beaconLayouts.removeAllElements();
            beaconManager.bind(this);
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    public void stopSearching() {
        try{
            if (beaconManager != null) beaconManager.unbind(this);
            started = false;
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }



    @Override
    public void onBeaconServiceConnect() {
        try{
            beaconManager.setMonitorNotifier(this);
            beaconManager.setRangeNotifier(this);

            regions = new Vector<>();
            try {
                for(String uuid: uuids){
                    Region r = new Region(uuid, Identifier.parse(uuid), null, null);
                    regions.add(r);
                    beaconManager.startMonitoringBeaconsInRegion(r);
                }
            } catch (RemoteException e) {
            }
            started = true;
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }

    @Override
    public Context getApplicationContext() {
        return mContext;
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {
        try {
            if(started){
                for(Region region: regions){
                    beaconManager.stopMonitoringBeaconsInRegion(region);
                }
            }
        } catch (RemoteException e) {
        }finally {
            regions = new Vector<>();
            mContext.unbindService(serviceConnection);
            started = false;
            mMain.onBeaconInRange(new ArrayList<Beacon>());
        }
    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        try{
            return mContext.bindService(intent, serviceConnection, i);
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
            return false;
        }
    }

    private int enteredRegionsCount=0;
    @Override
    public void didEnterRegion(Region region) {
        try {
            if(!regions.contains(region)){
                return;
            }
            enteredRegionsCount++;
            beaconManager.startRangingBeaconsInRegion(region);
        } catch (RemoteException e) {
        }
    }

    @Override
    public void didExitRegion(Region region) {
        try {
            enteredRegionsCount--;
            mLastFoundBeacons = new ArrayList<Collection<Beacon>>(MAXEVENTS+1);
            beaconManager.stopRangingBeaconsInRegion(region);
            if(enteredRegionsCount==0) {
                //Log.d("didExitRegion", mLastFoundBeacons.toString());
                mMain.onBeaconInRange(new ArrayList<Beacon>());
            }
        } catch (RemoteException e) {

        }
    }

    @Override
    public void didDetermineStateForRegion(int i, Region region) {

    }


    private Collection<Beacon> calcBestBeaconRanging(Collection<Beacon> beacons, Region region) {
        try{
            if (mLastFoundBeacons == null) {
                mLastFoundBeacons = new ArrayList<>(MAXEVENTS+1);
                mLastFoundBeacons.add(beacons);
                return beacons;
            }

            Collection<Beacon> bestList = beacons;
            for (Collection<Beacon> bHistory : mLastFoundBeacons) {
                bestList = (bHistory != null && bHistory.size() > bestList.size() ? bHistory : bestList);
            }
            mLastFoundBeacons.add(0, beacons);

            if(mLastFoundBeacons.size()>MAXEVENTS){
                mLastFoundBeacons.remove(mLastFoundBeacons.size()-1);
            }
            return bestList;
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
            return null;
        }
    }

    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
        try{
            if(region !=null && !regions.contains(region)){
                return;
            }
            if(beacons.size()==0 && enteredRegionsCount>1) return;
                mMain.onBeaconInRange(beacons);
        }catch (Exception ex){
            Log.e(TAG,"Unknown Error: ",ex);
        }
    }
}