package de.gamesthm.s3.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Class to create an alert dialog
 *
 * @author Dustin Leibnitz
 */
public class Alert{


    public void alertOK(String title, String message, Context con) {
        new AlertDialog.Builder(con)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
}
